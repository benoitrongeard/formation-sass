// Requis
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var csscomb = require('gulp-csscomb');

// Include plugins
var plugins = require('gulp-load-plugins')();

var source = './node_modules';
var dir = './public';
var path_css = './public/css';
var path_scss = './public/scss';

//Fonction pour inclure Boostrap
gulp.task('bootstrap', function(){
    return gulp.src(source + '/bootstrap/dist/css/*.min.css')
        .pipe(gulp.dest(path_css + '/libs/bootstrap'));
});

//Fonction pour compiler SASS en CSS
gulp.task('sass', function(){
    return gulp.src(path_scss + '/**/*.scss')
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(csscomb())
        .pipe(gulp.dest(path_css))
        .pipe(browserSync.reload({
          stream: true
        }))
});

gulp.task('watch', ['browserSync', 'sass'],function(){
    gulp.watch(path_scss + '/**/*.scss', ['sass']);
    gulp.watch(dir + '/**/*.html', browserSync.reload);
});

gulp.task('browserSync', function() {
    browserSync({
        server: {
          baseDir: 'public'
        },
    })
})

// Tâche "build"
gulp.task('build', ['bootstrap', 'watch']);