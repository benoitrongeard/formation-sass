# Formation SASS

Dépôt pour la formation SASS du Tech-Game

### Prérequis

* [NodeJs](https://nodejs.org/en/)

### Installation
``` shell
$ npm install gulp -g
$ npm run build
```

### Information
* Un fichier de correction avec le css est disponible, il suffit d'ajouter le css "correction.css" dans l'index.html

### Imbrication
``` sass
div {
	font-size: 32px;
	h1 {
		color: green;
		font-weight: bold;
		&:hover {
			color: red;
			cursor: pointer;
		}
	}
}
```

### Héritage
``` sass
.info {
	padding: 10px;
	border: 1px solid #999;
	border-radius: 5px;
	margin: 10px 0px;
}

.error {
	@extend .info;
	border-color: #870000;
	background-color: #FFD9D9;
}

.success {
	@extend .error;
	border-color: #005700;
	background-color: #D6FFD6;
}
```

### Import
``` sass
@import "reset.scss";
@import "color.scss";
```

### Variables
``` sass
$red: #FFD9D9;
$green: #D6FFD6;
```

### Fonction et mixins
``` sass
@function borderColor($color) {
	@return desaturate(darken($color, 30%), 70%)
}

@mixin border($color : white) {
	border-color: borderColor($color);
	background-color: $color;
}
```
* On peut par la suite utiliser notre fonction dans une classe : 
``` sass
border-color: borderColor($color);
```
* De la même manière, on peut utiliser notre mixin dans une classe : 
``` sass
@include border($red);
```

### Les conditions
``` sass
@if $btnColor == black {
	color: white;
} @else {
	color: black;
}
```

### Les boucles
``` sass
@for $i from 1 to $columns + 1 {
	.column#{$i} {
		@extend .column;
		width: ($i * $width) / $columns - (2 * 1%);
	}	
}
```